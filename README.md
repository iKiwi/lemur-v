# LEMUR-V

LEMUR-V is a lazy emulator for risc-v 32I ISA.


## Features

- Decodes rv32I instructions into their assembly
- Supports risc-v registers and memory model
- Emulating binaries

## Models
Currently the most simple architecture model `MarkI` is implemented. 

`MarkI` -- Just stores images of CPU and RAM. User can access them via read/write and load/store methods. 

## Installation

> Note: LEMUR-V requires `-std=c++23` option (gcc >= 13.1).

You can install the newest version of gcc [here](https://gcc.gnu.org/install/download.html)

On Ubuntu run:
```sh
sudo apt install gcc-13 g++-13
```

To build the project:
```sh
git clone https://gitlab.com/iKiwi/lemur-v.git
cd lemur-v
git submodule init
git submodule update
mkdir build && cd build
cmake ..
make
```

## Compilation Flags

`-DWITH_TESTS=ON`  enables testing

## Artifacts
In the build directory:

`src/decoder_toy` -- Console apk to decode rv32I instructions

`src/lemu` -- Emulator 

`tests/run_test_*` -- Runs test

`snippy/MarkI` -- MarkI model interface (shared lib)

> Note: After installation you can define the PATH variable to access lemu or decoder-toy if you wish (recomended).

## Usage

A simple hello-world programm can be found in the /samples directory. This is how a memory image can be created:

```sh
# assemble:
riscv64-unknown-linux-gnu-as -march=rv32i -mabi=ilp32 -o hello.o hello.S
# link:
riscv64-unknown-linux-gnu-ld -march=rv32i -m elf32lriscv_ilp32 -o hello.elf hello.o
# elf -> bin
riscv64-unknown-linux-gnu-objcopy -O binary hello.elf hello.bin
```

After running this shell script just pass hello.bin to lemu:

```sh
lemu hello.bin
```
## Verification

LEMU was verificated via [syntacore's llvm-snippy code generator](https://github.com/syntacore/snippy/tree/main). To test `MarkI` navigate into the `build/snippy` directory. Then run:

```sh
llvm-snippy --model-plugin=./MarkI <path/to/yaml>
```

You can find an yml configuration file `layout.yaml` with all instructions included in `snippy/yml` directory. 

## License

GNU GPLv3



