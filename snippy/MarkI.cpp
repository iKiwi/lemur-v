#include "headers/VTable.h"

#include "Models.hpp"
#include "Devices.hpp"
#include "Lemu.hpp"

#include <iostream>

using namespace Lemu;

unsigned char RVMAPI_VERSION_SYMBOL = RVMAPI_CURRENT_INTERFACE_VERSION;

struct RVMState
{
    Models::MarkI Model;
    Models::MarkI const *operator ->() const noexcept { return &Model; }
    Models::MarkI       *operator ->()       noexcept { return &Model; }
};

using MarkI = RVMState;

MarkI *rvm_modelCreate(const RVMConfig *config)
{
    std::cout << "Creating MarkI" << std::endl;

    auto const Size = std::max
    (
        config->RomStart + config->RomSize,
        config->RamStart + config->RamSize
    );

    return new MarkI({Devices::CPU{}, Devices::RAM(Size, b8{})});
}

const RVMConfig *rvm_getModelConfig(const MarkI *State)
{
    return new RVMConfig{};
}

void rvm_modelDestroy(MarkI *State)
{
    std::cout << "Destroying MarkI" << std::endl;

    delete State;
}

int rvm_executeInstr(MarkI *State) try
{
    LEMU emu{std::move(State->Model)};
    auto const ebreak = emu.updateState() ? 0 : 1;
    State->Model = std::move(emu.getState());

    return ebreak;
}
catch(std::runtime_error &e) { std::cout << e.what() << std::endl; return -1; }
catch(std::exception     &e) { std::cout << e.what() << std::endl; return -1; }
catch(...)
{
    std::cout << "Caught an unrecognized exception" << std::endl; return -1;
}

void rvm_readMem(const MarkI *State, uint64_t Addr, size_t Count, char *Data)
{
    std::for_each_n(Data, Count, [&](char       &n){ n = (*State)->loadB(Addr++); });
}

void rvm_writeMem(MarkI *State, uint64_t Addr, size_t Count, const char *Data)
{
    std::for_each_n(Data, Count, [&](const char &n){ (*State)->storeB(Addr++, n); });
}

uint64_t rvm_readPC(const MarkI *State){ return (*State)->read(pc); }

void rvm_setPC(MarkI *State, uint64_t NewPC)
{
    (*State)->write(pc, static_cast<b32>(NewPC));
}

RVMRegT rvm_readXReg(const RVMState *State, RVMXReg Reg)
{
    return (*State)->read(static_cast<Register>(Reg));
}

void rvm_setXReg(RVMState *State, RVMXReg Reg, RVMRegT Value)
{
    (*State)->write
    (
        static_cast<Register>(Reg),
        static_cast<b32>(Value)
    );
}


RVMRegT rvm_readFReg(const RVMState *State, RVMFReg Reg)                         { return 0; }
void rvm_setFReg(RVMState *State, RVMFReg Reg, RVMRegT Value)                    {           }
RVMRegT rvm_readCSRReg(const RVMState *State, unsigned Reg)                      { return 0; }
void rvm_setCSRReg(RVMState *State, unsigned Reg, RVMRegT Value)                 {           }
int rvm_readVReg(const RVMState *State, RVMVReg Reg, char *Data, size_t MaxSize) { return 0; }
int rvm_setVReg(RVMState *State, RVMVReg Reg, const char *Data, size_t DataSize) { return 0; }
int rvm_queryCallbackSupportPresent() { return 0; }

void rvm_logMessage(const char *Message) { std::cout << Message << std::endl; }



extern const rvm::RVM_FunctionPointers RVMAPI_ENTRY_POINT_SYMBOL
{
    .modelCreate = &rvm_modelCreate,
    .modelDestroy = &rvm_modelDestroy,

    .getModelConfig = &rvm_getModelConfig,

    .executeInstr = &rvm_executeInstr,

    .readMem = &rvm_readMem,
    .writeMem = &rvm_writeMem,

    .readPC = &rvm_readPC,
    .setPC = &rvm_setPC,

    .readXReg = &rvm_readXReg,
    .setXReg = &rvm_setXReg,

    .readFReg = &rvm_readFReg,
    .setFReg = &rvm_setFReg,

    .readCSRReg = &rvm_readCSRReg,
    .setCSRReg = &rvm_setCSRReg,

    .readVReg = &rvm_readVReg,
    .setVReg = &rvm_setVReg,

    .logMessage = &rvm_logMessage,
    .queryCallbackSupportPresent = &rvm_queryCallbackSupportPresent,
};



