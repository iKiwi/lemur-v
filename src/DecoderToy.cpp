#include "Decode.hpp"

#include <iostream>
#include <iomanip>
#include <limits>

int main()
{
    using namespace Lemu;

    b32 instruction;

    while (true)
    {
        std::cout << "Enter a binary RV32I instruction in hex: " << std::endl;
        std::cout << "Input in hex: ";

        if(std::cin >> std::hex >> instruction)
        {
            auto const I = Decode::parseBinInstr(instruction);
            if (I.has_value())
            {
                std::cout << std::endl;
                std::cout << "Parser result:     "  << std::endl;
                std::visit(
                        [](auto&& instr)
                        {
                            std::cout << instr.  toStr() << std::endl;
                            std::cout << instr.typeStr() << std::endl;
                        }, *I);
            } else
            {
                std::cout << I.error() << ", try again." << std::endl;
            }
        } else if (std::cin.eof())
        {
            std::cout << "\nExiting" << std::endl;
            break;
        } else
        {
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            std::cout << "\nInvalid input, try again." << std::endl;
        }

        std::cout << std::endl;
    }

    return 0;
}
