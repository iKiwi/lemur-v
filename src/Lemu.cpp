#include "Lemu.hpp"
#include "Models.hpp"
#include "Devices.hpp"

#include <fstream>
#include <iostream>

using namespace Lemu;

int main(int argc, char* argv[])
{
    try
    {
        if (argc != 2)
        {
            std::cout << "Usage:"               << std::endl;
            std::cout << "       lemu [infile]" << std::endl;
            return 1;
        }

        auto const file = argv[1];

        std::ifstream binaryInput(file);
        if (!binaryInput.is_open())
        {
            std::cout << "lemu: "  << file << ": " << "No such file or directory" << std::endl;
            return 1;
        }

        std::vector<b8> memoryImage{std::istreambuf_iterator<char>(binaryInput), {}};

        Devices::RAM ram{std::move(memoryImage)};
        Devices::CPU cpu{};

        Models::MarkI model{std::move(cpu), std::move(ram)};

        LEMU emu{std::move(model)};

        emu.execute();
    }
    catch(std::runtime_error &e) { std::cout << e.what() << std::endl; }
    catch(std::exception     &e) { std::cout << e.what() << std::endl; }
    catch(...)
    {
        std::cout << "Caught an unrecognized exception" << std::endl;
    }

    return 0;
}
