#pragma once

#include "Types.hpp"

#include <stdexcept>
#include <expected>
#include <unordered_map>

namespace Lemu
{
    namespace Decode
    {
        enum RTypeI
        {
            ADD, SUB, SLL, SLT, SLTU, XOR, SRL, SRA, OR, AND
        };
        constexpr std::array<std::string, 10>
        RTypeIStr =
        {
            "add", "sub", "sll", "slt", "sltu",
            "xor", "srl", "sra", "or" , "and"
        };


        enum ITypeI
        {
            ADDI, SLLI , SLTI  , SLTIU, XORI, ORI, ANDI,
            SRLI, SRAI , LB    , LH   , LW  , LBU, LHU ,
            JALR,

            ECALL, EBREAK, FENCE
        };
        constexpr std::array<std::string, 18>
        ITypeIStr =
        {
            "addi", "slli", "slti", "sltiu", "xori", "ori", "andi",
            "srli", "srai", "lb"  , "lh"   , "lw"  , "lbu", "lhu" ,
            "jalr",

            "ecall", "ebreak", "fence"
        };


        enum STypeI
        {
            SB, SH, SW
        };
        constexpr std::array<std::string, 3>
        STypeIStr =
        {
            "sb", "sh", "sw"
        };


        enum BTypeI
        {
            BEQ, BNE, BLT, BGE, BLTU, BGEU
        };
        constexpr std::array<std::string, 6>
        BTypeIStr =
        {
            "beq", "bne", "blt", "bge", "bltu", "bgeu"
        };


        enum UTypeI { LUI, AUIPC };
        constexpr std::array<std::string, 2>
        UTypeIStr = { "lui", "auipc" };


        enum JTypeI {JAL};
        constexpr std::array<std::string, 1>
        JTypeIStr = { "jal" };

    } // namespace Decode

    namespace Decode
    {
        constexpr b32  OP_MASK  = 0x0000007f;
        constexpr b32  RD_MASK  = 0x00000f80;
        constexpr b32  R1_MASK  = 0x000f8000;
        constexpr b32  R2_MASK  = 0x01f00000;
        constexpr b32  F3_MASK  = 0x00007000;
        constexpr b32  F7_MASK  = 0xfe000000;
        constexpr b32 SMT_MASK  = 0x01f00000;

        constexpr b32 IMM12_MASK = 0xfff00000;
        constexpr b32 IMM20_MASK = 0xfffff000;
    }

    namespace Decode
    {
        constexpr b32      fetchOP(b32 const instr) noexcept
        {
            return apply<OP_MASK>(instr);
        }

        constexpr Register fetchRD(b32 const instr) noexcept
        {
            return Register
            (
                   apply<RD_MASK>(instr)
            );
        }

        constexpr Register fetchR1(b32 const instr) noexcept
        {
            return Register
            (
                   apply<R1_MASK>(instr)
            );
        }

        constexpr Register fetchR2(b32 const instr) noexcept
        {
            return Register
            (
                   apply<R2_MASK>(instr)
            );
        }

        constexpr b8       fetchF3(b32 const instr) noexcept
        {
            return apply<F3_MASK>(instr);
        }

        constexpr b8       fetchF7(b32 const instr) noexcept
        {
            return apply<F7_MASK>(instr);
        }

        constexpr b8      fetchSMT(b32 const instr) noexcept
        {
            return apply<SMT_MASK>(instr);
        }

        constexpr i32    fetchIMMB(b32 const instr) noexcept
        {
            b32 const s1 = (instr >> 20) & 0xfe0;
            b32 const s2 = (instr >>  7) & 0x01f;
            b32 const sf = s1 | s2;

            b32 const b  = sf % 2u
                      ? (sf & 0xffe) | 0x800
                      : (sf & 0xffe) & 0x7ff;

            b32 const e  = (instr >> 31)
                                ? 0xfffff000
                                : 0x00000000;
            return toI32(b | e);
        }

        constexpr i32    fetchIMMI(b32 const instr) noexcept
        {
            b32 const immediate = apply<IMM12_MASK>(instr);
            b32 const res = signExtend<12>(immediate);

            return toI32(res);
        }

        constexpr i32    fetchIMMJ(b32 const instr) noexcept
        {
            b32 const sf = apply<IMM12_MASK>(instr);

            b32 const b =  sf % 2u
                        ? (sf & 0xffe) | 0x800
                        : (sf & 0xffe) & 0x7ff;

            b32 const m  = instr & 0x000ff000;

            b32 const e  = (instr >> 31)
                                ? 0xfff00000
                                : 0x00000000;

            return toI32(b|m|e);
        }

        constexpr i32    fetchIMMS(b32 const instr) noexcept
        {
            b32 const b = apply<RD_MASK>(instr);
            b32 const m = apply<F7_MASK>(instr, 5u);
            b32 const e = signExtend<12>(m);

            return toI32(b|e);
        }

        constexpr i32    fetchIMMU(b32 const instr) noexcept
        {
            b32 const immediate = apply<IMM20_MASK>(instr);
            b32 const res = signExtend<20>(immediate);

            return toI32(res);
        }

        constexpr std::expected<RTypeI, Error>
        fetchIR(b32 const instr) noexcept
        {
            b8 const f3 = fetchF3(instr);
            b8 const f7 = fetchF7(instr);

            static std::unordered_map<b8, RTypeI>
            const RTypeIMap
            {
                {0b0000000 + 0b000, RTypeI::ADD },
                {0b0100000 + 0b000, RTypeI::SUB },
                {0b0000000 + 0b001, RTypeI::SLL },
                {0b0000000 + 0b010, RTypeI::SLT },
                {0b0000000 + 0b011, RTypeI::SLTU},
                {0b0000000 + 0b100, RTypeI::XOR },
                {0b0000000 + 0b101, RTypeI::SRL },
                {0b0100000 + 0b101, RTypeI::SRA },
                {0b0000000 + 0b110, RTypeI::OR  },
                {0b0000000 + 0b111, RTypeI::AND },
            };

            if (RTypeIMap. contains(f3 + f7))
                return RTypeIMap.at(f3 + f7);

            return std::unexpected
            (
                "Invalid R-type Instruction"
            );
        }

        constexpr std::expected<ITypeI, Error>
        fetchIII(b32 const instr) noexcept
        {
            b32  const opcode = fetchOP(instr);

            if (opcode == 0b1100111)
                return ITypeI::JALR;

            bool const isLoad = (opcode == 0b0000011);
            b8   const f3     = fetchF3 (instr);


            if (isLoad)
            {
                static std::unordered_map<b8, ITypeI>
                const ITypeIMap
                {
                    {0b000, ITypeI::LB },
                    {0b001, ITypeI::LH },
                    {0b010, ITypeI::LW },
                    {0b100, ITypeI::LBU},
                    {0b101, ITypeI::LHU},
                };

                if (ITypeIMap. contains(f3))
                    return ITypeIMap.at(f3);
            }
            else
            {
                static std::unordered_map<b8, ITypeI>
                const ITypeIMap
                {
                    {0b000, ITypeI::ADDI },
                    {0b010, ITypeI::SLTI },
                    {0b011, ITypeI::SLTIU},
                    {0b100, ITypeI::XORI },
                    {0b110, ITypeI::ORI  },
                    {0b111, ITypeI::ANDI },
                };

                if (ITypeIMap. contains(f3))
                    return ITypeIMap.at(f3);
            }

            return std::unexpected
            (
                "Invalid I-type Instruction"
            );
        }

        constexpr std::expected<ITypeI, Error>
        fetchIIS(b32 const instr) noexcept
        {
            b8 const f3 = fetchF3(instr);
            b8 const f7 = fetchF7(instr);

            static std::unordered_map<b8, ITypeI>
            const ITypeIMap
            {
                {0b0000000 + 0b001, ITypeI::SLLI},
                {0b0000000 + 0b101, ITypeI::SRLI},
                {0b0100000 + 0b101, ITypeI::SRAI},
            };

            if (ITypeIMap. contains(f7 + f3))
                return ITypeIMap.at(f7 + f3);

            return std::unexpected
            (
                "Invalid I-type Instruction"
            );
        }

        constexpr std::expected<STypeI, Error>
        fetchIS(b32 const instr) noexcept
        {
            b8 const f3 = fetchF3(instr);

            static std::unordered_map<b8, STypeI>
            const STypeIMap
            {
                {0b000, STypeI::SB},
                {0b001, STypeI::SH},
                {0b010, STypeI::SW},
            };

            if (STypeIMap. contains(f3))
                return STypeIMap.at(f3);

            return std::unexpected
            (
                "Invalid S-type Instruction"
            );
        }

        constexpr std::expected<BTypeI, Error>
        fetchIB(b32 const instr) noexcept
        {
            b8 const f3 = fetchF3(instr);

            static std::unordered_map<b8, BTypeI>
            const BTypeIMap
            {
                {0b000, BTypeI::BEQ },
                {0b001, BTypeI::BNE },
                {0b100, BTypeI::BLT },
                {0b101, BTypeI::BGE },
                {0b110, BTypeI::BLTU},
                {0b111, BTypeI::BGEU},
            };

            if (BTypeIMap. contains(f3))
                return BTypeIMap.at(f3);

            return std::unexpected
            (
                 "Invalid B-type Instruction"
            );
        }

        constexpr std::expected<UTypeI, Error>
        fetchIU(b32 const instr) noexcept
        {
            b32 const opcode = fetchOP(instr);

            static std::unordered_map<b32, UTypeI>
            const UTypeIMap
            {
                {0b0110111, UTypeI::LUI  },
                {0b0010111, UTypeI::AUIPC},
            };

            if (UTypeIMap. contains(opcode))
                return UTypeIMap.at(opcode);

            return std::unexpected
            (
                "Invalid U-type Instruction"
            );
        }

    } // namespace Decode


    namespace Decode
    {
        struct RType; struct IType; struct SType;
        struct BType; struct UType; struct JType;

        using Instruction = std::variant<
                                         RType,
                                         IType,
                                         SType,
                                         BType,
                                         UType,
                                         JType
                                        >;

        using ExpectedI = std::expected<Instruction, Error>;

        struct RType
        {
            Register     rd;
            Register r1, r2;
            RTypeI        i;

            static constexpr ExpectedI decode(b32  const b) noexcept;
            static constexpr std::string typeStr()          noexcept { return "R-type"; }
                   constexpr std::string   toStr() const    noexcept;
                   constexpr std::string   opStr() const    noexcept { return RTypeIStr[i]; }
        };

        struct IType
        {
            i32         imm;
            Register     rd;
            Register     r1;
            ITypeI        i;

            static constexpr ExpectedI decode(b32  const b) noexcept;
            static constexpr std::string typeStr()          noexcept { return "I-type"; }
                   constexpr std::string   toStr() const    noexcept;
                   constexpr std::string   opStr() const    noexcept { return ITypeIStr[i]; }
        };

        struct SType
        {
            i32         imm;
            Register r1, r2;
            STypeI        i;

            static constexpr ExpectedI decode(b32  const b) noexcept;
            static constexpr std::string typeStr()          noexcept { return "S-type"; }
                   constexpr std::string   toStr() const    noexcept;
                   constexpr std::string   opStr() const    noexcept { return STypeIStr[i]; }
        };

        struct BType
        {
            i32         imm;
            Register r1, r2;
            BTypeI        i;

            static constexpr ExpectedI decode(b32  const b) noexcept;
            static constexpr std::string typeStr()          noexcept { return "B-type"; }
                   constexpr std::string   toStr() const    noexcept;
                   constexpr std::string   opStr() const    noexcept { return BTypeIStr[i]; }
        };

        struct UType
        {
            i32         imm;
            Register     rd;
            UTypeI        i;

            static constexpr ExpectedI decode(b32  const b) noexcept;
            static constexpr std::string typeStr()          noexcept { return "U-type"; }
                   constexpr std::string   toStr() const    noexcept;
                   constexpr std::string   opStr() const    noexcept { return UTypeIStr[i]; }
        };

        struct JType
        {
            i32         imm;
            Register     rd;
            JTypeI        i;

            static constexpr ExpectedI decode(b32  const b) noexcept;
            static constexpr std::string typeStr()          noexcept { return "J-type"; }
                   constexpr std::string   toStr() const    noexcept;
                   constexpr std::string   opStr() const    noexcept { return JTypeIStr[i]; }
        };

    } // namespace Decode


    namespace Decode
    {
        constexpr ExpectedI RType::decode(b32 const b) noexcept
        {
            if (fetchIR(b).has_value())
                return RType
                {
                    .rd  = fetchRD(b),
                    .r1  = fetchR1(b),
                    .r2  = fetchR2(b),
                    .i   = fetchIR(b).value(),
                };

            return std::unexpected
            (
                fetchIR(b).error()
            );
        }

        constexpr ExpectedI IType::decode(b32 const b) noexcept
        {
            b32  const opcode  = fetchOP(b);

            if (opcode == 0b1110011)
                return fetchIMMI(b)
                    ? IType{ .i = ITypeI::EBREAK }
                    : IType{ .i = ITypeI::ECALL  };

            if (opcode == 0b0001111)
                return IType{ .i = ITypeI::FENCE };

            b8   const f3      = fetchF3 (b);
            bool const shamt = ((opcode == 0b0010011) && (f3 == 0b001 || f3 == 0b101));

            if (shamt)
            {
                if (fetchIIS(b).has_value())
                    return IType
                    {
                        .imm = fetchSMT (b),
                        .rd  = fetchRD  (b),
                        .r1  = fetchR1  (b),
                        .i   = fetchIIS (b).value(),
                    };
            }
            else
            {
                if(fetchIII(b).has_value())
                    return IType
                    {
                        .imm = fetchIMMI(b),
                        .rd  = fetchRD  (b),
                        .r1  = fetchR1  (b),
                        .i   = fetchIII (b).value(),
                    };
            }

            return shamt
                ? std::unexpected(fetchIIS(b).error())
                : std::unexpected(fetchIII(b).error());
        }

        constexpr ExpectedI SType::decode(b32 const b) noexcept
        {
            if(fetchIS(b).has_value())
                return SType
                {
                    .imm = fetchIMMS(b),
                    .r1  = fetchR1  (b),
                    .r2  = fetchR2  (b),
                    .i   = fetchIS  (b).value(),
                };

            return std::unexpected
            (
                fetchIS(b).error()
            );
        }

        constexpr ExpectedI BType::decode(b32 const b) noexcept
        {
            if (fetchIB(b).has_value())
                return BType
                {
                    .imm = fetchIMMB(b),
                    .r1  = fetchR1  (b),
                    .r2  = fetchR2  (b),
                    .i   = fetchIB  (b).value(),
                };

            return std::unexpected
            (
                fetchIB(b).error()
            );
        }

        constexpr ExpectedI UType::decode(b32 const b) noexcept
        {
            if (fetchIU(b).has_value())
                return  UType
                {
                    .imm = fetchIMMU(b),
                    .rd  = fetchRD  (b),
                    .i   = fetchIU  (b).value(),
                };

            return std::unexpected
            (
                fetchIU(b).error()
            );
        }

        constexpr ExpectedI JType::decode(b32 const b) noexcept
        {
            return JType
            {
                .imm = fetchIMMJ(b),
                .rd  = fetchRD  (b),
                .i   = {}
            };
        }

    } // namespace Decode


    namespace Decode
    {
        constexpr std::string RType::toStr() const noexcept
        {
            return              RTypeIStr[  i] +
                    " x" + std::to_string( rd) +
                   ", x" + std::to_string( r1) +
                   ", x" + std::to_string( r2) ;
        }

        constexpr std::string IType::toStr() const noexcept
        {
            if  (i >= ITypeI::ECALL)
            {
                return ITypeIStr[i];
            }
            else if (i < ITypeI::LB)
            {
                return              ITypeIStr[  i] +
                        " x" + std::to_string( rd) +
                       ", x" + std::to_string( r1) +
                       ", "  + std::to_string(imm) ;
            }
            else
            {
                return              ITypeIStr[  i] +
                        " x" + std::to_string( rd) +
                        ", " + std::to_string(imm) +
                        "(x" + std::to_string( r1) +
                        ")"                        ;
            }
        }

        constexpr std::string UType::toStr() const noexcept
        {
            return             UTypeIStr[  i] +
                   " x" + std::to_string( rd) +
                   ", " + std::to_string(imm) ;
        }

        constexpr std::string SType::toStr() const noexcept
        {
            return              STypeIStr[  i] +
                   " x"  + std::to_string( r2) +
                   ", "  + std::to_string(imm) +
                   "(x"  + std::to_string( r1) +
                   ")"                         ;
        }

        constexpr std::string BType::toStr() const noexcept
        {
            return              BTypeIStr[  i] +
                    " x" + std::to_string( r1) +
                   ", x" + std::to_string( r2) +
                   ", "  + std::to_string(imm) ;
        }

        constexpr std::string JType::toStr() const noexcept
        {
            return              JTypeIStr[  i] +
                   " x" + std::to_string( rd) +
                   ", " + std::to_string(imm) ;
        }

    } // namespace Decode


    using Decode::Instruction;
    namespace Decode
    {

        constexpr ExpectedI parseBinInstr(b32 const b) noexcept
        {
            using decodeF = decltype(+[](b32)->ExpectedI { return {}; });

            static std::unordered_map<b32, decodeF>
            const DecodeMap
            {
                {0b0000011, IType::decode}, // LOAD
                {0b0010011, IType::decode}, // IMMOP
                {0b0010111, UType::decode}, // AUIPC
                {0b0100011, SType::decode}, // STORE
                {0b0110011, RType::decode}, // REGOP
                {0b0110111, UType::decode}, // LUI
                {0b1100011, BType::decode}, // BR
                {0b1100111, IType::decode}, // JALR
                {0b1101111, JType::decode}, // JAL
                {0b1110011, IType::decode}, // SYS
                {0b0001111, IType::decode}, // FENCE
            };

            auto const op = fetchOP(b);

            if (DecodeMap. contains(op))
                return DecodeMap.at(op)(b);

            return std::unexpected
            (
                "Invalid RV32I instruction"
            );
        }

    } // namespace Decode

} // namespace Lemu
