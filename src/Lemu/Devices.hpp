#pragma once

#include <array>
#include <vector>
#include <ranges>
#include <algorithm>

#include "Types.hpp"

namespace Lemu
{
    namespace Devices
    {
        class RegisterDevice
        {
            // Holds registers x0, x1, x2, ...., x31, and pc.
            using registers = std::array<b32, 33>;
            registers reg{};

        public:
            RegisterDevice(){}

            template<std::forward_iterator I, std::sentinel_for<I> S>
            RegisterDevice(I first, S last)
            {
                assert(std::ranges::distance(first, last) == 33);
                std::ranges::copy(first, last, reg.begin());
                reg[x0] = 0; // x0 hardwired to zero
            }
            template<std::ranges::forward_range R>
            RegisterDevice(R && range) : RegisterDevice(std::ranges::begin(range),
                                                        std::ranges::end  (range)) {}
        public:
            b32 read      (Register const which) const noexcept;
            b32 operator[](Register const which) const noexcept;

        public:
            void write(Register const which, b32 const val) noexcept;

        public:
            using const_iterator = registers::const_iterator;

            const_iterator begin() const noexcept { return reg.cbegin(); }
            const_iterator end  () const noexcept { return reg.cend  (); }
        };

        b32 RegisterDevice::read      (Register const which) const noexcept { return reg[which]; }
        b32 RegisterDevice::operator[](Register const which) const noexcept { return reg[which]; }


        void RegisterDevice::write(Register const which, b32 const val) noexcept
        {
            if (which != x0) // x0 hardwired to zero
                reg[which] = val;
        }

    } // namespace Devices


    namespace Devices
    {
        class MemoryDevice
        {
            using memory = std::vector<b8>;
            memory mem{};

        public:
            template <typename ... Args>
            MemoryDevice(Args&& ... args) : mem(std::forward<Args>(args)...) {}

        public:
            std::size_t size() { return mem.size(); }

        public:
            b8  loadB(std::size_t const) const noexcept;
            b16 loadH(std::size_t const) const;
            b32 loadW(std::size_t const) const;

        public:
            b32 operator[](std::size_t const I) const { return loadW(I); }

        public:
            void storeB(std::size_t const, b8  const) noexcept;
            void storeH(std::size_t const, b16 const);
            void storeW(std::size_t const, b32 const);

        public:
            using const_iterator = memory::const_iterator;

            const_iterator begin() const noexcept { return mem.cbegin(); }
            const_iterator end  () const noexcept { return mem.cend  (); }
        };

        b8  MemoryDevice::loadB(std::size_t const J) const noexcept
        {
            return mem[J];
        }

        b16 MemoryDevice::loadH(std::size_t const J) const
        {
            if (J % HWORD != 0)
                throw( std::runtime_error("Access Fault") );

            return fromByteRangeTo<b16>(std::next(mem.begin(), J));
        }

        b32 MemoryDevice::loadW(std::size_t const J) const
        {
            if (J %  WORD != 0)
                throw( std::runtime_error("Access Fault") );

            return fromByteRangeTo<b32>(std::next(mem.begin(), J));
        }

        void MemoryDevice::storeB(std::size_t const J, b8  const val) noexcept
        {
            mem[J] = val;
        }

        void MemoryDevice::storeH(std::size_t const J, b16 const val)
        {
            if (J % HWORD != 0)
                throw( std::runtime_error("Access Fault") );

            auto const I = std::bit_cast<b8 *>(&val);
            auto const O = std::next(mem.begin(), J);
            std::copy(I, I + HWORD, O);
        }

        void MemoryDevice::storeW(std::size_t const J, b32 const val)
        {
            if (J % WORD != 0)
                throw( std::runtime_error("Access Fault") );

            auto const I = std::bit_cast<b8 *>(&val);
            auto const O = std::next(mem.begin(), J);
            std::copy(I, I + WORD, O);
        }

    } // namespace Devices

    namespace Devices
    {
        using CPU = RegisterDevice; // simpliest CPU model, registers only
        using RAM =   MemoryDevice;
    } // namespace Devices

} // namespace Lemu
