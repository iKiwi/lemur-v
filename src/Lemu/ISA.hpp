#pragma once

#include "Models.hpp"
#include "Decode.hpp"

#include <exception>
#include <unistd.h>

namespace Lemu
{

    namespace ISA
    {

        constexpr u32 WD = WORD; // shortcut for better code-style

        constexpr bool BREAK    = false;
        constexpr bool CONTINUE =  true;



        template <Model M>
        constexpr auto ADD = [](M & m, Instruction const I) noexcept
        {
            auto const & [rd, r1, r2, i] = std::get<Decode::RType>(I);

            u32 const R1 = toU32(m.read(r1));
            u32 const R2 = toU32(m.read(r2));
            u32 const PC = toU32(m.read(pc));

            m.write(rd, toB32(R1 + R2));
            m.write(pc, toB32(PC + WD));

            return CONTINUE;
        };

        template <Model M>
        constexpr auto SUB = [](M & m, Instruction const I) noexcept
        {
            auto const & [rd, r1, r2, i] = std::get<Decode::RType>(I);

            u32 const R1 = toU32(m.read(r1));
            u32 const R2 = toU32(m.read(r2));
            u32 const PC = toU32(m.read(pc));

            m.write(rd, toB32(R1 - R2));
            m.write(pc, toB32(PC + WD));

            return CONTINUE;
        };

        template <Model M>
        constexpr auto SLT = [](M & m, Instruction const I) noexcept
        {
            auto const & [rd, r1, r2, i] = std::get<Decode::RType>(I);

            i32  const R1 = toI32(m.read(r1));
            i32  const R2 = toI32(m.read(r2));
            u32  const PC = toU32(m.read(pc));

            m.write(rd, toB32(R1 < R2));
            m.write(pc, toB32(PC + WD));

            return CONTINUE;
        };

        template <Model M>
        constexpr auto SLTU = [](M & m, Instruction const I) noexcept
        {
            auto const & [rd, r1, r2, i] = std::get<Decode::RType>(I);

            u32  const R1 = toU32(m.read(r1));
            u32  const R2 = toU32(m.read(r2));
            u32  const PC = toU32(m.read(pc));

            m.write(rd, toB32(R1 < R2));
            m.write(pc, toB32(PC + WD));

            return CONTINUE;
        };

        template <Model M>
        constexpr auto AND = [](M & m, Instruction const I) noexcept
        {
            auto const & [rd, r1, r2, i] = std::get<Decode::RType>(I);

            u32 const R1 = toU32(m.read(r1));
            u32 const R2 = toU32(m.read(r2));
            u32 const PC = toU32(m.read(pc));

            m.write(rd, toB32(R1 & R2));
            m.write(pc, toB32(PC + WD));

            return CONTINUE;
        };

        template <Model M>
        constexpr auto OR = [](M & m, Instruction const I) noexcept
        {
            auto const & [rd, r1, r2, i] = std::get<Decode::RType>(I);

            u32 const R1 = toU32(m.read(r1));
            u32 const R2 = toU32(m.read(r2));
            u32 const PC = toU32(m.read(pc));

            m.write(rd, toB32(R1 | R2));
            m.write(pc, toB32(PC + WD));

            return CONTINUE;
        };

        template <Model M>
        constexpr auto XOR = [](M & m, Instruction const I) noexcept
        {
            auto const & [rd, r1, r2, i] = std::get<Decode::RType>(I);

            u32 const R1 = toU32(m.read(r1));
            u32 const R2 = toU32(m.read(r2));
            u32 const PC = toU32(m.read(pc));

            m.write(rd, toB32(R1 ^ R2));
            m.write(pc, toB32(PC + WD));

            return CONTINUE;
        };

        template <Model M>
        constexpr auto SLL = [](M & m, Instruction const I) noexcept
        {
            auto const & [rd, r1, r2, i] = std::get<Decode::RType>(I);

            static constexpr b32 takeV = 0x1f;

            u32 const R1 = toU32(m.read(r1));
            u32 const SF = toU32(m.read(r2) & takeV);
            u32 const PC = toU32(m.read(pc));

            m.write(rd, toB32(R1 << SF));
            m.write(pc, toB32(PC +  WD));

            return CONTINUE;
        };

        template <Model M>
        constexpr auto SRL = [](M & m, Instruction const I) noexcept
        {
            auto const & [rd, r1, r2, i] = std::get<Decode::RType>(I);

            static constexpr b32 takeV = 0x1f;

            u32 const R1 = toU32(m.read(r1));
            u32 const SF = toU32(m.read(r2) & takeV);
            u32 const PC = toU32(m.read(pc));

            m.write(rd, toB32(R1 >> SF));
            m.write(pc, toB32(PC +  WD));

            return CONTINUE;
        };

        template <Model M>
        constexpr auto SRA = [](M & m, Instruction const I) noexcept
        {
            auto const & [rd, r1, r2, i] = std::get<Decode::RType>(I);

            static constexpr b32 takeV = 0x1f;

            i32 const R1 = toI32(m.read(r1));
            u32 const SF = toU32(m.read(r2) & takeV);
            u32 const PC = toU32(m.read(pc));

            m.write(rd, toB32(R1 >> SF));
            m.write(pc, toB32(PC +  WD));

            return CONTINUE;
        };

        template <Model M>
        constexpr auto ADDI = [](M & m, Instruction const I) noexcept
        {
            auto const & [imm, rd, r1, i] = std::get<Decode::IType>(I);

            i32 const IM = toI32(imm);

            i32 const R1 = toI32(m.read(r1));
            u32 const PC = toU32(m.read(pc));

            m.write(rd, toB32(R1 + IM));
            m.write(pc, toB32(PC + WD));

            return CONTINUE;
        };

        template <Model M>
        constexpr auto SLTI = [](M & m, Instruction const I) noexcept
        {
            auto const & [imm, rd, r1, i] = std::get<Decode::IType>(I);

            i32 const IM = toI32(imm);

            i32 const R1 = toI32(m.read(r1));
            u32 const PC = toU32(m.read(pc));

            m.write(rd, toB32(R1 < IM));
            m.write(pc, toB32(PC + WD));

            return CONTINUE;
        };

        template <Model M>
        constexpr auto SLTIU = [](M & m, Instruction const I) noexcept
        {
            auto const & [imm, rd, r1, i] = std::get<Decode::IType>(I);

            u32 const IM = toU32(imm);

            u32 const R1 = toU32(m.read(r1));
            u32 const PC = toU32(m.read(pc));

            m.write(rd, toB32(R1 < IM));
            m.write(pc, toB32(PC + WD));

            return CONTINUE;
        };

        template <Model M>
        constexpr auto ANDI = [](M & m, Instruction const I) noexcept
        {
            auto const & [imm, rd, r1, i] = std::get<Decode::IType>(I);

            i32 const IM = toI32(imm);

            i32 const R1 = toI32(m.read(r1));
            u32 const PC = toU32(m.read(pc));

            m.write(rd, toB32(R1 & IM));
            m.write(pc, toB32(PC + WD));

            return CONTINUE;
        };

        template <Model M>
        constexpr auto ORI = [](M & m, Instruction const I) noexcept
        {
            auto const & [imm, rd, r1, i] = std::get<Decode::IType>(I);

            i32 const IM = toI32(imm);

            i32 const R1 = toI32(m.read(r1));
            u32 const PC = toU32(m.read(pc));

            m.write(rd, toB32(R1 | IM));
            m.write(pc, toB32(PC + WD));

            return CONTINUE;
        };

        template <Model M>
        constexpr auto XORI = [](M & m, Instruction const I) noexcept
        {
            auto const & [imm, rd, r1, i] = std::get<Decode::IType>(I);

            i32 const IM = toI32(imm);

            i32 const R1 = toI32(m.read(r1));
            u32 const PC = toU32(m.read(pc));

            m.write(rd, toB32(R1 ^ IM));
            m.write(pc, toB32(PC + WD));

            return CONTINUE;
        };

        template <Model M>
        constexpr auto SLLI = [](M & m, Instruction const I) noexcept
        {
            auto const & [imm, rd, r1, i] = std::get<Decode::IType>(I);

            u32 const IM = toU32(imm);

            u32 const R1 = toU32(m.read(r1));
            u32 const PC = toU32(m.read(pc));

            m.write(rd, toB32(R1 << IM));
            m.write(pc, toB32(PC +  WD));

            return CONTINUE;
        };

        template <Model M>
        constexpr auto SRLI = [](M & m, Instruction const I) noexcept
        {
            auto const & [imm, rd, r1, i] = std::get<Decode::IType>(I);

            u32 const IM = toU32(imm);

            u32 const R1 = toU32(m.read(r1));
            u32 const PC = toU32(m.read(pc));

            m.write(rd, toB32(R1 >> IM));
            m.write(pc, toB32(PC +  WD));

            return CONTINUE;
        };

        template <Model M>
        constexpr auto SRAI = [](M & m, Instruction const I) noexcept
        {
            auto const & [imm, rd, r1, i] = std::get<Decode::IType>(I);

            u32 const IM = toU32(imm);

            i32 const R1 = toI32(m.read(r1));
            u32 const PC = toU32(m.read(pc));

            m.write(rd, toB32(R1 >> IM));
            m.write(pc, toB32(PC +  WD));

            return CONTINUE;
        };

        template <Model M>
        constexpr auto LUI = [](M & m, Instruction const I) noexcept
        {
            auto const & [imm, rd, i] = std::get<Decode::UType>(I);

            static constexpr u32 SF = 12u;

            u32 const IM = toU32(imm);
            u32 const PC = toU32(m.read(pc));

            m.write(rd, toB32(IM << SF));
            m.write(pc, toB32(PC +  WD));

            return CONTINUE;
        };

        template <Model M>
        constexpr auto AUIPC = [](M & m, Instruction const I) noexcept
        {
            auto const & [imm, rd, i] = std::get<Decode::UType>(I);

            static constexpr u32 SF = 12u;

            u32 const IM = toU32(imm) <<  SF;
            u32 const PC = toU32(m.read(pc));

            m.write(rd, toB32(PC + IM));
            m.write(pc, toB32(PC + WD));

            return CONTINUE;
        };

        template <Model M>
        constexpr auto JAL = [](M & m, Instruction const I) noexcept
        {
            auto const & [imm, rd, i] = std::get<Decode::JType>(I);

            u32 const IM = toU32(imm);
            u32 const PC = toU32(m.read(pc));

            m.write(rd, toB32(PC + WD));
            m.write(pc, toB32(PC + IM));

            return CONTINUE;
        };

        template <Model M>
        constexpr auto JALR = [](M & m, Instruction const I) noexcept
        {
            auto const & [imm, rd, r1, i] = std::get<Decode::IType>(I);

            static constexpr b32 zeroLSB = 0xfffffffe;

            u32 const IM = toU32(imm);

            u32 const R1 = toU32(m.read(r1));
            u32 const PC = toU32(m.read(pc));

            u32 const EA = R1 + IM;

            m.write(rd, toB32(PC + WD));
            m.write(pc, toB32(EA & zeroLSB));

            return CONTINUE;
        };

        template <Model M>
        constexpr auto BEQ = [](M & m, Instruction const I) noexcept
        {
            auto const & [imm, r1, r2, i] = std::get<Decode::BType>(I);

            u32 const IM = toU32(imm);

            u32 const R1 = toU32(m.read(r1));
            u32 const R2 = toU32(m.read(r2));
            u32 const PC = toU32(m.read(pc));

            if (R1 == R2)
                m.write(pc, PC + IM);
            else
                m.write(pc, PC + WD);

            return CONTINUE;
        };

        template <Model M>
        constexpr auto BNE = [](M & m, Instruction const I) noexcept
        {
            auto const & [imm, r1, r2, i] = std::get<Decode::BType>(I);

            u32 const IM = toU32(imm);

            u32 const R1 = toU32(m.read(r1));
            u32 const R2 = toU32(m.read(r2));
            u32 const PC = toU32(m.read(pc));

            if (R1 != R2)
                m.write(pc, PC + IM);
            else
                m.write(pc, PC + WD);

            return CONTINUE;
        };

        template <Model M>
        constexpr auto BLT = [](M & m, Instruction const I) noexcept
        {
            auto const & [imm, r1, r2, i] = std::get<Decode::BType>(I);

            u32 const IM = toU32(imm);

            i32 const R1 = toI32(m.read(r1));
            i32 const R2 = toI32(m.read(r2));
            u32 const PC = toU32(m.read(pc));

            if (R1 < R2)
                m.write(pc, PC + IM);
            else
                m.write(pc, PC + WD);

            return CONTINUE;
        };

        template <Model M>
        constexpr auto BGE = [](M & m, Instruction const I) noexcept
        {
            auto const & [imm, r1, r2, i] = std::get<Decode::BType>(I);

            u32 const IM = toU32(imm);

            i32 const R1 = toI32(m.read(r1));
            i32 const R2 = toI32(m.read(r2));
            u32 const PC = toU32(m.read(pc));

            if (R1 >= R2)
                m.write(pc, PC + IM);
            else
                m.write(pc, PC + WD);

            return CONTINUE;
        };

        template <Model M>
        constexpr auto BLTU = [](M & m, Instruction const I) noexcept
        {
            auto const & [imm, r1, r2, i] = std::get<Decode::BType>(I);

            u32 const IM = toU32(imm);

            u32 const R1 = toU32(m.read(r1));
            u32 const R2 = toU32(m.read(r2));
            u32 const PC = toU32(m.read(pc));

            if (R1 < R2)
                m.write(pc, PC + IM);
            else
                m.write(pc, PC + WD);

            return CONTINUE;
        };

        template <Model M>
        constexpr auto BGEU = [](M & m, Instruction const I) noexcept
        {
            auto const & [imm, r1, r2, i] = std::get<Decode::BType>(I);

            u32 const IM = toU32(imm);

            u32 const R1 = toU32(m.read(r1));
            u32 const R2 = toU32(m.read(r2));
            u32 const PC = toU32(m.read(pc));

            if (R1 >= R2)
                m.write(pc, PC + IM);
            else
                m.write(pc, PC + WD);

            return CONTINUE;
        };

        template <Model M>
        constexpr auto LW = [](M & m, Instruction const I)
        noexcept( noexcept(m.loadW(std::declval<std::size_t>())) )
        {
            auto const & [imm, rd, r1, i] = std::get<Decode::IType>(I);

            u32 const IM = toU32(imm);
            u32 const R1 = toU32(m.read(r1));
            u32 const PC = toU32(m.read(pc));

            b32 const value = m.loadW(R1 + IM);

            m.write(rd,   value);
            m.write(pc, PC + WD);

            return CONTINUE;
        };

        template <Model M>
        constexpr auto LHU = [](M & m, Instruction const I)
        noexcept( noexcept(m.loadW(std::declval<std::size_t>())) )
        {
            auto const & [imm, rd, r1, i] = std::get<Decode::IType>(I);

            u32 const IM = toU32(imm);
            u32 const R1 = toU32(m.read(r1));
            u32 const PC = toU32(m.read(pc));

            b32 const value = m.loadH(R1 + IM);

            m.write(rd,   value);
            m.write(pc, PC + WD);

            return CONTINUE;
        };

        template <Model M>
        constexpr auto LBU(M & m, Instruction const I)
        noexcept( noexcept(m.loadW(std::declval<std::size_t>())) )
        {
            auto const & [imm, rd, r1, i] = std::get<Decode::IType>(I);

            u32 const IM = toU32(imm);
            u32 const R1 = toU32(m.read(r1));
            u32 const PC = toU32(m.read(pc));

            b32 const value = m.loadB(R1 + IM);

            m.write(rd,   value);
            m.write(pc, PC + WD);

            return CONTINUE;
        };

        template <Model M>
        constexpr auto LH = [](M & m, Instruction const I)
        noexcept( noexcept(m.loadW(std::declval<std::size_t>())) )
        {
            auto const & [imm, rd, r1, i] = std::get<Decode::IType>(I);

            u32 const IM = toU32(imm);
            u32 const R1 = toU32(m.read(r1));
            u32 const PC = toU32(m.read(pc));

            b32 const value = m.loadH(R1 + IM);

            m.write(rd, signExtend<16>(value));
            m.write(pc, PC + WD);

            return CONTINUE;
        };

        template <Model M>
        constexpr auto LB = [](M & m, Instruction const I)
        noexcept( noexcept(m.loadW(std::declval<std::size_t>())) )
        {
            auto const & [imm, rd, r1, i] = std::get<Decode::IType>(I);

            u32 const IM = toU32(imm);
            u32 const R1 = toU32(m.read(r1));
            u32 const PC = toU32(m.read(pc));

            b32 const value = m.loadB(R1 + IM);

            m.write(rd, signExtend<8 >(value));
            m.write(pc, PC + WD);

            return CONTINUE;
        };

        template <Model M>
        constexpr auto SW = [](M & m, Instruction const I)
        noexcept( noexcept(m.loadW(std::declval<std::size_t>())) )
        {
            auto const & [imm, r1, r2, i] = std::get<Decode::SType>(I);

            u32 const IM = toU32(imm);
            u32 const R1 = toU32(m.read(r1));
            u32 const R2 = toU32(m.read(r2));
            u32 const PC = toU32(m.read(pc));

            b32 const value = b32(R2);

            m.storeW(R1 + IM, value);
            m.write(pc,     PC + WD);

            return CONTINUE;
        };

        template <Model M>
        constexpr auto SH = [](M & m, Instruction const I)
        noexcept( noexcept(m.loadW(std::declval<std::size_t>())) )
        {
            auto const & [imm, r1, r2, i] = std::get<Decode::SType>(I);

            u32 const IM = toU32(imm);
            u32 const R1 = toU32(m.read(r1));
            u32 const R2 = toU32(m.read(r2));
            u32 const PC = toU32(m.read(pc));

            b16 const value = b16(R2);

            m.storeH(R1 + IM, value);
            m.write(pc,     PC + WD);

            return CONTINUE;
        };

        template <Model M>
        constexpr auto SB = [](M & m, Instruction const I)
        noexcept( noexcept(m.loadW(std::declval<std::size_t>())) )
        {
            auto const & [imm, r1, r2, i] = std::get<Decode::SType>(I);

            u32 const IM = toU32(imm);
            u32 const R1 = toU32(m.read(r1));
            u32 const R2 = toU32(m.read(r2));
            u32 const PC = toU32(m.read(pc));

            b8  const value = b8(R2);

            m.storeB(R1 + IM, value);
            m.write(pc,     PC + WD);

            return CONTINUE;
        };

        template <Model M> // NOP
        constexpr auto FENCE = [](M & m, Instruction const I) noexcept
        {
            auto const & [imm, r1, r2, i] = std::get<Decode::IType>(I);

            u32 const PC = toU32(m.read(pc));
            m.write(pc, PC + WD);

            return CONTINUE;
        };

        template <Model M>
        constexpr auto EBREAK = [](M & m, Instruction const I) noexcept
        {
            return BREAK;
        };

        template <Model M>
        constexpr auto ECALL = [](M & m, Instruction const I)
        {
            b32 const A7 = m.read(x17);
            b32 const PC = m.read( pc);

            // todo: wrapp, fix reading
            switch (A7)
            {
                case 63:
                {
                    int         const fd    = m.read(x10);
                    std::size_t const I     = m.read(x11);
                    std::size_t const count = m.read(x12);

                    auto buff = new char[count];
                    std::size_t const N = ::read(fd, buff, count);
                    std::size_t J = I;
                    std::for_each_n(buff, std::min(count, N), [&](char &c) {  m.storeB(J++, c); });
                    delete [] buff;

                    m.write(pc , PC + WD);
                    m.write(x10,       N);
                    return CONTINUE;
                }
                case 64:
                {
                    int         const fd    = m.read(x10);
                    std::size_t const I     = m.read(x11);
                    std::size_t const count = m.read(x12);

                    auto buff = new char[count];
                    std::size_t J = I;
                    std::for_each(buff, buff + count, [&](char &c) { c = m.loadB(J++); });
                    std::size_t const N = ::write(fd, buff, count);
                    delete [] buff;

                    m.write(pc, PC + WD);
                    m.write(x10,      N);
                    return CONTINUE;
                }
                case 93:
                {
                    return BREAK;
                }
                default:
                {
                    throw std::runtime_error("Unrecognized ecall (may be not supported yet)");
                }
            }
        };

    } // namespace ISA


    namespace ISA
    {
        template <Model M>
        auto fetchInstrToExecOn(Instruction const I) noexcept
        {
            using execF = decltype(+[](M &, Instruction const) -> bool { return {}; });

            static std::unordered_map<std::string, execF>
            const OpcodeIMap =
            {
                {"add"  ,   +ADD<M>},
                {"sub"  ,   +SUB<M>},
                {"slt"  ,   +SLT<M>},
                {"sltu" ,  +SLTU<M>},
                {"and"  ,   +AND<M>},
                {"or"   ,    +OR<M>},
                {"xor"  ,   +XOR<M>},
                {"sll"  ,   +SLL<M>},
                {"srl"  ,   +SRL<M>},
                {"sra"  ,   +SRA<M>},
                {"addi" ,  +ADDI<M>},
                {"slti" ,  +SLTI<M>},
                {"sltiu", +SLTIU<M>},
                {"andi" ,  +ANDI<M>},
                {"ori"  ,   +ORI<M>},
                {"xori" ,  +XORI<M>},
                {"slli" ,  +SLLI<M>},
                {"srli" ,  +SRLI<M>},
                {"srai" ,  +SRAI<M>},
                {"lui"  ,   +LUI<M>},
                {"auipc", +AUIPC<M>},
                {"jal"  ,   +JAL<M>},
                {"jalr" ,  +JALR<M>},
                {"beq"  ,   +BEQ<M>},
                {"bne"  ,   +BNE<M>},
                {"blt"  ,   +BLT<M>},
                {"bge"  ,   +BGE<M>},
                {"bltu" ,  +BLTU<M>},
                {"bgeu" ,  +BGEU<M>},
                {"lw"   ,    +LW<M>},
                {"lh"   ,    +LH<M>},
                {"lb"   ,    +LB<M>},
                {"lhu"  ,   +LHU<M>},
                {"lbu"  ,   +LBU<M>},
                {"sw"   ,    +SW<M>},
                {"sh"   ,    +SH<M>},
                {"sb"   ,    +SB<M>},

                {"ecall" ,  +ECALL<M>},
                {"ebreak", +EBREAK<M>},
                {"fence" ,  +FENCE<M>},
            };

            std::string const opcode = std::visit([](auto && i){ return i.opStr(); }, I);
            return OpcodeIMap.at(opcode);
        }

    } // namespace ISA

} // namespace Lemu

