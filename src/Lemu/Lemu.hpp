#pragma once

#include "Models.hpp"
#include "ISA.hpp"
#include "Decode.hpp"

#include <utility>
#include <exception>

namespace Lemu
{
    template <Model M>
    class LEMU
    {
        M model;

    public:
        LEMU(M && from) : model(std::forward<M>(from)) {}

    public:
        M&        getState()       & noexcept { return model; }
        M const & getState() const & noexcept { return model; }

    public:
        void setState(M && state) { model = std::forward<M>(state); }

    public:
        bool updateState()
        {
            b32  const PC = model.read (pc);
            b32  const I  = model.loadW(PC);

            auto const decodedI = Decode::parseBinInstr(I);

            if(!decodedI.has_value())
                throw(std::runtime_error("Invalid instruction encountered"));

            auto const Instruction = *decodedI;
            auto const ExecuteOn   = ISA::fetchInstrToExecOn<M>(Instruction);
            return ExecuteOn(model, Instruction);
        }

        void execute(){ while(updateState()); }
    };

} // namespace Lemu
