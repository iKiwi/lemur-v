#pragma once

#include "Devices.hpp"
#include <concepts>
#include <utility>

namespace Lemu
{
    template <typename M>
    concept Model = requires (M model,  std::size_t I,  Register r)
    {
        { model.read (r) } -> std::same_as<b32>;

        { model.loadB(I) } -> std::same_as<b8 >;
        { model.loadH(I) } -> std::same_as<b16>;
        { model.loadW(I) } -> std::same_as<b32>;

        { model.write (r, std::declval<b32>()) };

        { model.storeB(I, std::declval<b8 >()) };
        { model.storeH(I, std::declval<b16>()) };
        { model.storeW(I, std::declval<b32>()) };
    };

    namespace Models
    {
        class MarkI
        {
            Devices::CPU cpu_;
            Devices::RAM ram_;

        public:
            MarkI(Devices::CPU &&cpu, Devices::RAM &&ram) : cpu_{std::move(cpu)},
                                                            ram_{std::move(ram)} {}

        public:
            Devices::CPU&        getCPUState()       & noexcept { return cpu_; }
            Devices::RAM&        getRAMState()       & noexcept { return ram_; }

            Devices::CPU const & getCPUState() const & noexcept { return cpu_; }
            Devices::RAM const & getRAMState() const & noexcept { return ram_; }

        public:
            b32 read (Register    const r) const noexcept
            {
                return cpu_.read (r);
            }

        public:
            b8  loadB(std::size_t const I) const noexcept(noexcept(ram_.loadB(I)))
            {
                return ram_.loadB(I);
            }

            b16 loadH(std::size_t const I) const noexcept(noexcept(ram_.loadH(I)))
            {
                return ram_.loadH(I);
            }

            b32 loadW(std::size_t const I) const noexcept(noexcept(ram_.loadW(I)))
            {
                return ram_.loadW(I);
            }

        public:
            void write (Register    const r, b32 const val) noexcept
            {
                cpu_.write(r, val);
            }

        public:
            void storeB(std::size_t const I, b8  const val) noexcept(noexcept(ram_.storeB(I, val)))
            {
                ram_.storeB(I, val);
            }
            void storeH(std::size_t const I, b16 const val) noexcept(noexcept(ram_.storeH(I, val)))
            {
                ram_.storeH(I, val);
            }
            void storeW(std::size_t const I, b32 const val) noexcept(noexcept(ram_.storeW(I, val)))
            {
                ram_.storeW(I, val);
            }

        };
    }

    static_assert(Model<Models::MarkI>);
}
