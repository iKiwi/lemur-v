#pragma once

#include <bit>
#include <array>
#include <string>
#include <cstdint>
#include <climits>
#include <cstddef>
#include <variant>
#include <algorithm>

namespace Lemu
{

    static_assert(CHAR_BIT == 8);

    using i32 =  int32_t;
    static_assert(sizeof(i32) == 4);

    using u32 = uint32_t;
    static_assert(sizeof(u32) == 4);
    using u16 = uint16_t;
    static_assert(sizeof(u16) == 2);
    using u8  = uint8_t;
    static_assert(sizeof(u8 ) == 1);

    using b32 = u32;
    using b16 = u16;
    using b8  =  u8;

    constexpr u32 BYTE         = 1u;
    constexpr u32 HWORD        = 2u;
    constexpr u32 WORD         = 4u;

    constexpr u32 BITS_IN_BYTE = 8u;


    constexpr b32 toB32(i32 const val) { return std::bit_cast<b32>(val); }
    constexpr b32 toB32(u32 const val) { return val; }

    constexpr i32 toI32(b32 const val) { return std::bit_cast<i32>(val); }
    constexpr i32 toI32(i32 const val) { return val; }

    constexpr u32 toU32(b32 const val) { return val; }
    constexpr u32 toU32(i32 const val) { return std::bit_cast<u32>(val); }

    constexpr u32  log2(u32 const val) noexcept { return val == 1u ? 0u : log2(val >> 1u) + 1u; }

    template <b32 mask>
    constexpr b32 apply(b32 const val, std::size_t offset = 0) noexcept
    {
        u32 const shift = mask == 0u ? 0u : log2 (mask & -mask);
        return (val & mask) >> (shift - offset);
    }

    template <std::size_t nth_bit>
    constexpr b32 signExtend(b32 const val)
    {
        bool const signBit = (val >> (nth_bit - 1)) % 2;
        b32  const signMask = -1 << nth_bit;
        b32  const zeroMask = -signMask - 1;

        return signBit
        ? val | signMask
        : val & zeroMask;
    }

    template<typename To, std::forward_iterator It>
    constexpr To fromByteRangeTo(It const I, bool const bigEndian = false)
    {
        using From = typename  It::value_type;
        static_assert( sizeof(From) == BYTE );
        std::size_t const offset = sizeof(To);

        To res;

        if (!bigEndian)
            std::copy        (I, I + offset, std::bit_cast<From *>(&res));
        else
            std::reverse_copy(I, I + offset, std::bit_cast<From *>(&res));

        return res;
    }

    enum Register
    {
        x0 , x1 , x2 , x3 , x4 , x5 , x6 , x7 ,
        x8 , x9 , x10, x11, x12, x13, x14, x15,
        x16, x17, x18, x19, x20, x21, x22, x23,
        x24, x25, x26, x27, x28, x29, x30, x31,
        pc
    };


    using Error = std::string;

} // namespace Lemu
