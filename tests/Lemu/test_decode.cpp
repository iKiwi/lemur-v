#include <gtest/gtest.h>

#include "Decode.hpp"

using namespace Lemu;

std::string decode(b32 const instr)
{
    auto const I = Decode::parseBinInstr(instr);
    return I.has_value()
        ? std::visit([](auto&& instr){ return instr.toStr(); }, *I)
        : I.error();
}

TEST(DECODE, LUI)
{
    ASSERT_TRUE("lui x1, 10"      == decode(0x0000a0b7));
    ASSERT_TRUE("lui x31, -10"    == decode(0xffff6fb7));
    ASSERT_TRUE("lui x7, 0"       == decode(0x000003b7));
}

TEST(DECODE, AUIPC)
{
    ASSERT_TRUE("auipc x3, 119"   == decode(0x00077197));
    ASSERT_TRUE("auipc x17, -11"  == decode(0xffff5897));
    ASSERT_TRUE("auipc x0, 0"     == decode(0x00000017));
}

TEST(DECODE, JAL)
{
    ASSERT_TRUE("jal x3, 52"      == decode(0x034001ef));
    ASSERT_TRUE("jal x15, -102"   == decode(0xf9bff7ef));
    ASSERT_TRUE("jal x8, 0"       == decode(0x0000046f));
}

TEST(DECODE, JALR)
{
    ASSERT_TRUE("jalr x21, -2(x10)" == decode(0xffe50ae7));
    ASSERT_TRUE("jalr x26, 999(x9)" == decode(0x3e748d67));
    ASSERT_TRUE("jalr x26, 0(x9)"   == decode(0x00048d67));
}

TEST(DECODE, BEQ)
{
    ASSERT_TRUE("beq x10, x2, 18"   == decode(0x00250963));
    ASSERT_TRUE("beq x3, x21, -8"   == decode(0xff518ce3));
    ASSERT_TRUE("beq x9, x15, 0"    == decode(0x00f48063));
}

TEST(DECODE, BNE)
{
    ASSERT_TRUE("bne x25, x4, 44"   == decode(0x024c9663));
    ASSERT_TRUE("bne x22, x3, -46"  == decode(0xfc3b19e3));
    ASSERT_TRUE("bne x5, x12, 0"    == decode(0x00c29063));
}

TEST(DECODE, BLT)
{
    ASSERT_TRUE("blt x10, x14, 26"  == decode(0x00e54d63));
    ASSERT_TRUE("blt x8, x19, -50"  == decode(0xfd3447e3));
    ASSERT_TRUE("blt x2, x31, 0"    == decode(0x01f14063));
}

TEST(DECODE, BGE)
{
    ASSERT_TRUE("bge x18, x9, 128"  == decode(0x08995063));
    ASSERT_TRUE("bge x8, x16, -8"  == decode(0xff045ce3));
    ASSERT_TRUE("bge x11, x2, 0"   == decode(0x0025d063));
}

TEST(DECODE, BLTU)
{
    ASSERT_TRUE("bltu x12, x4, 32"  == decode(0x02466063));
    ASSERT_TRUE("bltu x10, x7, -56" == decode(0xfc7564e3));
    ASSERT_TRUE("bltu x2, x15, 0"   == decode(0x00f16063));
}


TEST(DECODE, BGEU)
{
    ASSERT_TRUE("bgeu x9, x8, 102" == decode(0x0684f363));
    ASSERT_TRUE("bgeu x31, x1, 20" == decode(0x001ffa63));
    ASSERT_TRUE("bgeu x15, x10, 4" == decode(0x00a7f263));
}

TEST(DECODE, LH)
{
    ASSERT_TRUE("lh x31, 20(x31)" == decode(0x014f9f83));
    ASSERT_TRUE("lh x3, 8(x12)"   == decode(0x00861183));
    ASSERT_TRUE("lh x31, 8(x8)"   == decode(0x00841f83));
}

TEST(DECODE, LW)
{
    ASSERT_TRUE("lw x10, 6(x30)"  == decode(0x006f2503));
    ASSERT_TRUE("lw x31, 256(x4)" == decode(0x10022f83));
    ASSERT_TRUE("lw x25, 2(x24)"  == decode(0x002c2c83));
}

TEST(DECODE, LBU)
{
    ASSERT_TRUE("lbu x25, 2(x24)"  == decode(0x002c4c83));
    ASSERT_TRUE("lbu x1, 512(x31)" == decode(0x200fc083));
    ASSERT_TRUE("lbu x7, 32(x21)"  == decode(0x020ac383));
}

TEST(DECODE, LHU)
{
    ASSERT_TRUE("lhu x14, 7(x15)"  == decode(0x0077d703));
    ASSERT_TRUE("lhu x26, 516(x3)" == decode(0x2041dd03));
    ASSERT_TRUE("lhu x4, 197(x30)" == decode(0x0c5f5203));
}

TEST(DECODE, SB )
{
    ASSERT_TRUE("sb x14, 7(x15)"   == decode(0x00e783a3));
    ASSERT_TRUE("sb x13, 512(x11)" == decode(0x20d58023));
    ASSERT_TRUE("sb x7, 345(x21)"  == decode(0x147a8ca3));
}

TEST(DECODE, SH)
{
    ASSERT_TRUE("sh x7, 345(x21)"  == decode(0x147a9ca3));
    ASSERT_TRUE("sh x13, 512(x11)" == decode(0x20d59023));
    ASSERT_TRUE("sh x1, 765(x31)"  == decode(0x2e1f9ea3));
}

TEST(DECODE, SW)
{
    ASSERT_TRUE("sw x1, 765(x31)"  == decode(0x2e1faea3));
    ASSERT_TRUE("sw x12, 7(x9)"    == decode(0x00c4a3a3));
    ASSERT_TRUE("sw x5, 128(x29)"  == decode(0x085ea023));
}

TEST(DECODE, ADDI)
{
    ASSERT_TRUE("addi x3, x25, 2047"  == decode(0x7ffc8193));
    ASSERT_TRUE("addi x31, x2, -2047" == decode(0x80110f93));
    ASSERT_TRUE("addi x9, x22, -234"  == decode(0xf16b0493));
}

TEST(DECODE, SLTI)
{
    ASSERT_TRUE("slti x9, x22, -234"   == decode(0xf16b2493));
    ASSERT_TRUE("slti x29, x18, -2040" == decode(0x80892e93));
    ASSERT_TRUE("slti x3, x1, 2040"    == decode(0x7f80a193));
}

TEST(DECODE, SLTIU)
{
    ASSERT_TRUE("sltu x3, x1, x23"  == decode(0x0170b1b3));
    ASSERT_TRUE("sltu x31, x15, x2" == decode(0x0027bfb3));
    ASSERT_TRUE("sltu x5, x17, x24" == decode(0x0188b2b3));
}

TEST(DECODE, XORI )
{
    ASSERT_TRUE("xori x5, x17, 2040"  == decode(0x7f88c293));
    ASSERT_TRUE("xori x25, x9, -2048" == decode(0x8004cc93));
    ASSERT_TRUE("xori x15, x9, 2047"  == decode(0x7ff4c793));
}

TEST(DECODE, ORI)
{
    ASSERT_TRUE("ori x15, x9, 2047"  == decode(0x7ff4e793));
    ASSERT_TRUE("ori x16, x4, -2048" == decode(0x80026813));
    ASSERT_TRUE("ori x26, x14, 3"    == decode(0x00376d13));
}

TEST(DECODE, ANDI)
{
    ASSERT_TRUE("andi x26, x14, 3"     == decode(0x00377d13));
    ASSERT_TRUE("andi x31, x1, 2047"   == decode(0x7ff0ff93));
    ASSERT_TRUE("andi x27, x21, -2048" == decode(0x800afd93));
}

TEST(DECODE, SLLI)
{
    ASSERT_TRUE("slli x27, x21, 31" == decode(0x01fa9d93));
    ASSERT_TRUE("slli x7, x2, 1"    == decode(0x00111393));
    ASSERT_TRUE("slli x10, x20, 5"  == decode(0x005a1513));
}

TEST(DECODE, SRLI)
{
    ASSERT_TRUE("srli x10, x20, 21" == decode(0x015a5513));
    ASSERT_TRUE("srli x10, x2, 17"  == decode(0x01115513));
    ASSERT_TRUE("srli x17, x31, 1"  == decode(0x001fd893));
}

TEST(DECODE, SRAI)
{
    ASSERT_TRUE("srai x17, x31, 12" == decode(0x40cfd893));
    ASSERT_TRUE("srai x27, x3, 19"  == decode(0x4131dd93));
    ASSERT_TRUE("srai x27, x31, 1"  == decode(0x401fdd93));
}

TEST(DECODE, ADD)
{
    ASSERT_TRUE("add x31, x4, x5"   == decode(0x00520fb3));
    ASSERT_TRUE("add x30, x14, x1"  == decode(0x00170f33));
    ASSERT_TRUE("add x31, x24, x10" == decode(0x00ac0fb3));
}

TEST(DECODE, SUB)
{
    ASSERT_TRUE("sub x30, x24, x10" == decode(0x40ac0f33));
    ASSERT_TRUE("sub x31, x4, x1"   == decode(0x40120fb3));
    ASSERT_TRUE("sub x31, x25, x3"  == decode(0x403c8fb3));
}

TEST(DECODE, SLL)
{
    ASSERT_TRUE("sll x31, x25, x3" == decode(0x003c9fb3));
    ASSERT_TRUE("sll x27, x31, x1" == decode(0x001f9db3));
    ASSERT_TRUE("sll x7, x31, x11" == decode(0x00bf93b3));
}

TEST(DECODE, SLT )
{
    ASSERT_TRUE("slt x7, x31, x11" == decode(0x00bfa3b3));
    ASSERT_TRUE("slt x17, x31, x1" == decode(0x001fa8b3));
    ASSERT_TRUE("slt x19, x3, x31" == decode(0x01f1a9b3));
}

TEST(DECODE, SLTU)
{
    ASSERT_TRUE("sltu x19, x3, x31" == decode(0x01f1b9b3));
    ASSERT_TRUE("sltu x9, x31, x23" == decode(0x017fb4b3));
    ASSERT_TRUE("sltu x31, x1, x25" == decode(0x0190bfb3));
}

TEST(DECODE, XOR)
{
    ASSERT_TRUE("xor x31, x1, x23" == decode(0x0170cfb3));
    ASSERT_TRUE("xor x9, x31, x2"  == decode(0x002fc4b3));
    ASSERT_TRUE("xor x31, x23, x2" == decode(0x002bcfb3));
}

TEST(DECODE, SRL)
{
    ASSERT_TRUE("srl x31, x23, x2" == decode(0x002bdfb3));
    ASSERT_TRUE("srl x4, x9, x31"  == decode(0x01f4d233));
    ASSERT_TRUE("srl x27, x9, x31" == decode(0x01f4ddb3));
}

TEST(DECODE, SRA)
{
    ASSERT_TRUE("sra x27, x9, x31"  == decode(0x41f4ddb3));
    ASSERT_TRUE("sra x2, x29, x31"  == decode(0x41fed133));
    ASSERT_TRUE("sra x21, x31, x30" == decode(0x41efdab3));
}

TEST(DECODE, OR)
{
    ASSERT_TRUE("or x21, x31, x30" == decode(0x01efeab3));
    ASSERT_TRUE("or x21, x3, x31"  == decode(0x01f1eab3));
    ASSERT_TRUE("or x31, x30, x3"  == decode(0x003f6fb3));
}

TEST(DECODE, AND )
{
    ASSERT_TRUE("and x31, x30, x3"  == decode(0x003f7fb3));
    ASSERT_TRUE("and x23, x30, x31" == decode(0x01ff7bb3));
    ASSERT_TRUE("and x27, x31, x3"  == decode(0x003ffdb3));
}
