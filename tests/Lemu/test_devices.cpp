#include <gtest/gtest.h>

#include "Devices.hpp"

using namespace Lemu;

TEST(CPU_MODEL, default_init)
{
    Devices::CPU cpu;

    for(auto && regState : cpu)
        ASSERT_EQ(regState, 0u);
}

TEST(CPU_MODEL, range_init)
{
    std::vector<b32> state = {
                              6u, 1u, 4u, 1u, 2u, 4u, 7u, 1u,
                              7u, 3u, 8u, 6u, 2u, 8u, 4u, 6u,
                              3u, 9u, 0u, 4u, 7u, 2u, 9u, 5u,
                              3u, 7u, 1u, 2u, 6u, 9u, 8u, 8u,
                              1926u
                             };

    Devices::CPU cpu(state);

    for(std::size_t I{}; auto && regState : cpu)
    {
        if (I == 0)
            ASSERT_EQ(regState, 0u); // x0 is hardwired to zero
        else
            ASSERT_EQ(regState, state[I]);
        ++I;
    }
}

TEST(CPU_MODEL, iterator_init)
{
    std::vector<b32> state = {
                              6u, 1u, 4u, 1u, 2u, 4u, 7u, 1u,
                              7u, 3u, 8u, 6u, 2u, 8u, 4u, 6u,
                              3u, 9u, 0u, 4u, 7u, 2u, 9u, 5u,
                              3u, 7u, 1u, 2u, 6u, 9u, 8u, 8u,
                              1926u
                             };

    Devices::CPU cpu(state.begin(), state.end());

    for(std::size_t I{}; auto && regState : cpu)
    {
        if (I == 0)
            ASSERT_EQ(regState, 0u); // x0 is hardwired to zero
        else
            ASSERT_EQ(regState, state[I]);
        ++I;
    }
}

TEST(CPU_MODEL, read_write)
{
    Devices::CPU cpu;

    cpu.write(x1, toB32(-10));
    cpu.write(x3,  8u);
    cpu.write(pc, 99u);
    cpu.write(x2, toI32(cpu[x1]) + toI32(cpu[x3]));
    cpu.write(x0, 1239173u);

    ASSERT_EQ(      cpu[pc] , 99u);
    ASSERT_EQ(toI32(cpu[x1]), -10);
    ASSERT_EQ(toI32(cpu[x2]),  -2);
    ASSERT_EQ( cpu.read(x3),   8u);
    ASSERT_EQ( cpu.read(x0),   0u);
}

TEST(RAM_MODEL, size_init)
{
    Devices::RAM ram(12);
    ASSERT_EQ(ram.size(), 12);

    for(auto && data : ram)
        ASSERT_EQ(data, 0u);
}

TEST(RAM_MODEL, move_init)
{
    std::vector<b8> state = {9, 81, 2, 0, 52, 34, 14, 45, 73, 31};
    std::vector<b8> res   = state;
    Devices::RAM ram{std::move(state)};

    for(auto I = res.begin(); auto && data : ram)
    {
        ASSERT_EQ(data, *I);
        ++I;
    }
}

TEST(RAM_MODEL, iterator_init)
{
    std::vector<b8> state = {9, 81, 2, 0, 52, 34, 14, 45, 73, 31};

    Devices::RAM ram{state.begin(), state.end()};

    for(auto I = state.begin(); auto && data : ram)
    {
        ASSERT_EQ(data, *I);
        ++I;
    }
}

TEST(RAM_MODEL, load_store)
{
    Devices::RAM ram(20);

    ram.storeW(0u, 0x1100f324);
    ram.storeB(2u, 0xab      );

    ASSERT_EQ(ram.loadB(2u), 0xab      );
    ASSERT_EQ(ram.loadW(0u), 0x11abf324);

    ram.storeH(4u, 0x1337);
    ram.storeH(6u, 0x110b);

    ASSERT_EQ(ram.loadW(4u), 0x110b1337);

    ram.storeW(8u , 0x9d16fab4);
    ram.storeH(10u, 0x45ab    );
    ram.storeB(11u, 0xac      );

    ASSERT_EQ(ram.loadH(10u), 0xacab);
    ASSERT_EQ(ram.loadB(8u ), 0xb4  );
    ASSERT_EQ(ram.loadB(9u ), 0xfa  );

    ram.storeB(13u, 0x74  );
    ram.storeH(14u, 0xa91d);
    ram.storeB(12u, 0xbc  );

    ASSERT_EQ(ram.loadW(12u), 0xa91d74bc);

    ASSERT_THROW(ram.storeH(17u, 0x700b    ), std::runtime_error);
    ASSERT_THROW(ram.storeW(18u, 0x01234103), std::runtime_error);
}

