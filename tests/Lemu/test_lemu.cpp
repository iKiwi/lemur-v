#include <gtest/gtest.h>

#include "Models.hpp"
#include "ISA.hpp"
#include "Lemu.hpp"

using namespace Lemu;

#define UPDATE emu = std::move(model); emu.updateState(); model = std::move(emu.getState())
#define reg(x) model.read((x))

TEST(LEMU, ADD)
{
    Models::MarkI model{Devices::CPU{}, Devices::RAM(3u)}; // init model

    model.write(x19,  1); // writes into x19  1
    model.write(x6 ,  4); // writes into x6   4
    model.write(x14, -7); // writes into x14 -7
    model.write(x21,  4); // writes into x21  4

    model.storeW(0u, 0x006980b3); // add x1, x19, x6
    model.storeW(4u, 0x015703b3); // add x7, x14, x21
    model.storeW(8u, 0x00038233); // mov x4, x7

    LEMU UPDATE;
    ASSERT_EQ(reg(x1), 0x00000005); // x1 == 5
    ASSERT_EQ(reg(pc), 0x00000004); // pc == 4

    UPDATE;
    ASSERT_EQ(reg(x7), 0xfffffffd); // x7 == -3
    ASSERT_EQ(reg(pc), 0x00000008); // pc ==  8

    UPDATE;
    ASSERT_EQ(reg(x4),    reg(x7)); // x4 == x7
    ASSERT_EQ(reg(pc), 0x0000000c); // pc == 12
}

TEST(LEMU, SUB)
{
    Models::MarkI model{Devices::CPU{}, Devices::RAM(3)}; // init model


    model.write(x31, 0xffffffff);// writes into x31 -1
    model.write(x17, 0x00000004);// writes into x17 4
    model.write(x19, 0xffffffff);// writes into x19 -1
    model.write(x10, 0x00000023);// writes into x10 35
    model.write(x3 , 0x00000000);// writes into x3 0
    model.write(x21, 0x00000001);// writes into x21 1
    model.write(x19, 0xffffffff);// writes into x19  -1
    model.write(x14, 0x00000000);// writes into x14 0
    model.write(x15, 0x80000000);// writes into x15 -2147483648
    model.write(x16, 0x80000000);// writes into x16 -2147483648
    model.write(x2 , 0x7fffffff);// writes into x2 2147483647


    ASSERT_EQ(model.read(x15), model.read(x16)); //just check
    ASSERT_EQ(model.read(x19), model.read(x31));
    ASSERT_EQ(model.read(x19), -1);
    ASSERT_EQ(model.read(x31), -1);//otladka


    model.storeW(0u, 0x41310733);//sub x14, x2, x19
    model.storeW(4u, 0x413f8ab3 );//sub x21, x31, x19 aka -1 - (-1)
    model.storeW(8u, 0x41f988b3);//sub x17, x19, x31
    model.storeW(12u,0x40a101b3);//sub x3, x2, x10 ( 2147483647 - 35)


    LEMU UPDATE;
    ASSERT_EQ(model.read(x14),  model.read(x16));//x14 == x16 (2147483647-(-1) == -2147483648)
    UPDATE;
    ASSERT_EQ(model.read(x21), toB32(0)); //x21 ==0
    UPDATE;
    ASSERT_EQ(model.read(x17), model.read(x21)); //x17==x21 (-1 - (-1) == 0)
    UPDATE;
    ASSERT_EQ(model.read(x3), 0x7fffffdc); // x3 == 2147483612
    ASSERT_EQ(reg(x17),  0); //x17 ==0


    ASSERT_EQ(model.read(pc), 16);//pc == 4*4=16
}

TEST(LEMU, ADDI)
{
    Models::MarkI model{Devices::CPU{}, Devices::RAM(5u)}; // init model

    model.write(x18, 0xa73b721f); // writes into x18 -1489276385
    model.write(x17, 0x4e5a91be); // writes into x14  1314558398
    model.write(x4 , 0xfffffffe); // writes into x4  -2

    model.storeW(0u , 0x38690293); // addi x5 , x18,  902
    model.storeW(4u , 0x00b20d93); // addi x27, x4 ,   11
    model.storeW(8u , 0xee988313); // addi x6 , x17, -279
    model.storeW(12u, 0x00090c93); // mov  x25, x18
    model.storeW(16u, 0x24f00213); // mov  x4 , 591

    LEMU UPDATE;
    ASSERT_EQ(reg(x5), 0xa73b75a5); // x5 == -1489275483
    ASSERT_EQ(reg(pc), 0x00000004); // pc == 4

    UPDATE;
    ASSERT_EQ(reg(x27), 0x00000009); // x27 == 9
    ASSERT_EQ(reg(pc),  0x00000008); // pc  == 8

    UPDATE;
    ASSERT_EQ(reg(x6), 0x4e5a90a7); // x6 == 1314558119
    ASSERT_EQ(reg(pc), 0x0000000c); // pc == 12

    UPDATE;
    ASSERT_EQ(reg(x25),   reg(x18)); // x7 == -3
    ASSERT_EQ(reg(pc),  0x00000010); // pc == 16

    UPDATE;
    ASSERT_EQ(reg(x4), 0x0000024f); // x4 == 591
    ASSERT_EQ(reg(pc), 0x00000014); // pc == 20
}

TEST(LEMU, SLTU)
{
    Models::MarkI model{Devices::CPU{}, Devices::RAM(5)}; // init model

    model.write(x19,  1); // writes into x19 1
    model.write(x6 ,  4); // writes into x6  4
    model.write(x17,  -1);
    model.write(x14,  -10);
    model.write(x16,  54);
    model.write(x9 ,  45);
    model.write(x5 ,  45);

    model.write(x31 ,  45);//rd
    model.write(x30,  54);//rd



    model.storeW(0u, 0x01333833); // sltu x16, x6, x19
    model.storeW(4u, 0x0069b4b3); // sltu x9, x19, x6
    model.storeW(8u, 0x006332b3); // sltu x5, x6, x6
    model.storeW(12u, 0x00e8bfb3);//sltu x31, x17, x14
    model.storeW(16u, 0x0068bf33);//sltu x30, x17, x6



    LEMU UPDATE;

    ASSERT_EQ(model.read(x16),  0);
    UPDATE;
    ASSERT_EQ(model.read(x9), 1);
    UPDATE;
    ASSERT_EQ(model.read(x5), 0);
    UPDATE;
    ASSERT_EQ(model.read(x31),  0);
    UPDATE;

    ASSERT_EQ(model.read(x30),  0);

    ASSERT_EQ(model.read(x5), 0);

    ASSERT_EQ(model.read(pc), 20);

}

TEST(LEMU, SLTI)
{
    Models::MarkI model{Devices::CPU{}, Devices::RAM(5u)}; // init model

    model.write(x23, 0xfffffea1); // writes into x23 -351
    model.write(x16, 0x000007e8); // writes into x16 2024

    model.storeW(0u , 0xe5fba493); // slti x9 , x23, -417
    model.storeW(4u , 0xee7ba793); // slti x15, x23, -281
    model.storeW(8u , 0x0c782113); // slti x2 , x16,  199
    model.storeW(12u, 0x7f682a13); // slti x20, x16, 2038
    model.storeW(16u, 0xffc02093); // slti x1 , x0 , -4

    LEMU UPDATE;
    ASSERT_EQ(reg(x9), false); // x5 == 0
    ASSERT_EQ(reg(pc), 0x004); // pc == 4

    UPDATE;
    ASSERT_EQ(reg(x15), true); // x15 == 1
    ASSERT_EQ(reg(pc),  0x08); // pc  == 8

    UPDATE;
    ASSERT_EQ(reg(x2), false); // x2 == 0
    ASSERT_EQ(reg(pc), 0x00c); // pc == 12

    UPDATE;
    ASSERT_EQ(reg(x20), true); // x20 == 1
    ASSERT_EQ(reg(pc),  0x10); // pc  == 16

    UPDATE;
    ASSERT_EQ(reg(x1), false); // x1 == 0
    ASSERT_EQ(reg(pc), 0x014); // pc == 20
}


TEST(LEMU, ANDI)
{
    Models::MarkI model{Devices::CPU{}, Devices::RAM(4u)}; // init model

    model.write(x11, 0xffffffa1); // writes into x11 -95
    model.write(x24, 0x00000315); // writes into x24 789

    model.storeW(0u , 0x4a35fa93); // andi x21, x11, 1187
    model.storeW(4u , 0xc195f293); // andi x5 , x11, -999
    model.storeW(8u , 0x4d1c7e93); // andi x29, x24, 1233
    model.storeW(12u, 0x3ae2f093); // andi x1 , x5 ,  942

    LEMU UPDATE;
    ASSERT_EQ(reg(x21), 0x000004a1); // x21 == 1185
    ASSERT_EQ(reg(pc),  0x00000004); // pc  == 4

    UPDATE;
    ASSERT_EQ(reg(x5), 0xfffffc01); // x5 == -1023
    ASSERT_EQ(reg(pc), 0x00000008); // pc == 8

    UPDATE;
    ASSERT_EQ(reg(x29), 0x00000011); // x29 == 17
    ASSERT_EQ(reg(pc),  0x0000000c); // pc  == 12

    UPDATE;
    ASSERT_EQ(reg(x1),    reg(x0)); // x1 == 0
    ASSERT_EQ(reg(pc), 0x00000010); // pc == 16
}

TEST(LEMU, ORI)
{
    Models::MarkI model{Devices::CPU{}, Devices::RAM(4u)}; // init model

    model.write(x29, 0xfffff928); // writes into x29 -1752
    model.write(x18, 0x00000243); // writes into x18  579

    model.storeW(0u , 0x6d7eed13); // ori x26, x29, 1751
    model.storeW(4u , 0xdbc96393); // ori x7 , x18, -580
    model.storeW(8u , 0x16e06793); // mov x15, 366
    model.storeW(12u, 0x24396913); // ori x18, x18,  579

    LEMU UPDATE;
    ASSERT_EQ(reg(x26), 0xffffffff); // x26 == -1
    ASSERT_EQ(reg(pc),  0x00000004); // pc  ==  4

    UPDATE;
    ASSERT_EQ(reg(x7), 0xffffffff); // x7 == -1
    ASSERT_EQ(reg(pc), 0x00000008); // pc == 8

    UPDATE;
    ASSERT_EQ(reg(x15), 0x0000016e); // x15 == 366
    ASSERT_EQ(reg(pc),  0x0000000c); // pc  == 12

    UPDATE;
    ASSERT_EQ(reg(x18), 0x00000243); // x18 == 579
    ASSERT_EQ(reg(pc),  0x00000010); // pc == 16

}

TEST(LEMU, BEQ)
{
    Models::MarkI model{Devices::CPU{}, Devices::RAM(10u)}; // init model

    model.write(x3 , 0x00bfc250); // writes into x3   12567120
    model.write(x19, 0xfffe0f70); // writes into x19  -7120
    model.write(x14, 0x00bfc250); // writes into x14  12567120

    model.storeW(0u , 0x01318363); // beq x3 , x19, 6
    model.storeW(4u , 0xff370fe3); // beq x14, x19, -2
    model.storeW(8u , 0x00e18663); // beq x14, x3 , 12
    model.storeW(20u, 0xfe000ee3); // beq x0 , x0 , -4

    LEMU UPDATE;
    ASSERT_EQ(reg(pc), 0x04); // pc == 4

    UPDATE;
    ASSERT_EQ(reg(pc), 0x08); // pc == 8

    UPDATE;
    ASSERT_EQ(reg(pc), 0x14); // pc == 20

    UPDATE;
    ASSERT_EQ(reg(pc), 0x10); // pc == 16
}

TEST(LEMU, AND)
{
    Models::MarkI model{Devices::CPU{}, Devices::RAM(4u)}; // init model

    model.write(x11, 0xaaaaaaaa); // writes into x11 1010 1010 1010...
    model.write(x24, 0x55555555); // writes into x24 0101 0101 0101...
    model.write(x13, 0xffffffff); // writes into x13 1111 1111 1111...
    model.write(x3 , 0x00000000); // writes into x3 0000 0000 0000...

    model.storeW(0u , 0x00bc7cb3); // and x25, x11, x24
    model.storeW(4u , 0x00dc7133); // and x2, x24, x13
    model.storeW(8u , 0x00d1f2b3); // and x5, x3, x13

    LEMU UPDATE;
    ASSERT_EQ(reg(x25), 0); // x25 == 0
    UPDATE;
    ASSERT_EQ(reg(x2),  reg(x24)); // x2 == x24
    UPDATE;
    ASSERT_EQ(reg(x5),  reg(x3)); // x5 == 0

}

TEST(LEMU, OR)
{
     Models::MarkI model{Devices::CPU{}, Devices::RAM(4u)}; // init model

    model.write(x11, 0xaaaaaaaa); // writes into x11 1010 1010 1010...
    model.write(x24, 0x55555555); // writes into x24 0101 0101 0101...
    model.write(x13, 0xffffffff); // writes into x13 1111 1111 1111...
    model.write(x3 , 0x00000000); // writes into x3 0000 0000 0000...

    model.storeW(0u , 0x0185ecb3); // or x25, x11, x24
    model.storeW(4u , 0x00dc6133); // or x2, x24, x13
    model.storeW(8u , 0x00b1e2b3); // or x5, x3, x11
    LEMU UPDATE;
    ASSERT_EQ(reg(x25), 0xffffffff); // x25 == 0
    UPDATE;
    ASSERT_EQ(reg(x2),  reg(x13)); // x2 == x24
    UPDATE;
    ASSERT_EQ(reg(x5),  reg(x11)); // x5 == 0

}

TEST(LEMU, XOR)
{
     Models::MarkI model{Devices::CPU{}, Devices::RAM(4u)}; // init model

    model.write(x11, 0xaaaaaaaa); // writes into x11 1010 1010 1010...
    model.write(x24, 0x55555555); // writes into x24 0101 0101 0101...
    model.write(x13, 0xffffffff); // writes into x13 1111 1111 1111...
    model.write(x3 , 0x00000000); // writes into x3 0000 0000 0000...

    model.storeW(0u , 0x0185ccb3); // xor x25, x11, x24
    model.storeW(4u , 0x00dc4133); // xor x2, x24, x13
    model.storeW(8u , 0x00b1c2b3); // xor x5, x3, x11
    LEMU UPDATE;
    ASSERT_EQ(reg(x25), 0xffffffff); // x25 == 0
    UPDATE;
    ASSERT_EQ(reg(x2),  reg(x11)); // x2 == x24
    UPDATE;
    ASSERT_EQ(reg(x5),  0xaaaaaaaa); // x5 == 0

}

TEST(LEMU, SLL)
{
    Models::MarkI model{Devices::CPU{}, Devices::RAM(8u)}; // init model
    //section for data for shift
    model.write(x10, 0); // writes into x11 0
    model.write(x11, 1); // writes into x24 1
    model.write(x12, 31 ); // writes into x13 32
    model.write(x13 , 5); // writes into x3  5
    model.write(x14 , 8); // writes into x3  5

    model.write(x20 , 0xaaaaaaaa); // writes into x20 1010 1010 1010...
    model.write(x21 , 0x55555555); // writes into x21 0101 0101 0101...
    model.write(x22 , 0x00000001); // writes into x22 0000 0000 0000 .... 0001

    model.storeW(0u , 0x00ea10b3 ); // sll x1, x20, x14
    model.storeW(4u , 0x00ca9133); // sll x2, x21, x12
    model.storeW(8u , 0x00b111b3); // sll x3, x2, x11
    model.storeW(12u , 0x00db1233); // sll x4, x22, x13

    LEMU UPDATE;
    ASSERT_EQ(reg(x1), 0xaaaaaa00);
    UPDATE;
    //std::cout << "0x"<<std::hex << reg(x2)<<std::endl;
    ASSERT_EQ(reg(x2), 0x80000000);
    UPDATE;
    ASSERT_EQ(reg(x3), 0x00000000);
    UPDATE;
    ASSERT_EQ(reg(x4), 0x00000020);

}

TEST(LEMU, SRL)
{
    Models::MarkI model{Devices::CPU{}, Devices::RAM(8u)}; // init model
    //section for data for shift
    model.write(x10, 0); // writes into x11 0
    model.write(x11, 1); // writes into x24 1
    model.write(x12, 31 ); // writes into x13 32
    model.write(x13 , 5); // writes into x3  5
    model.write(x14 , 8); // writes into x3  5

    model.write(x20 , 0xaaaaaaaa); // writes into x20 1010 1010 1010...
    model.write(x21 , 0x55555555); // writes into x21 0101 0101 0101...
    model.write(x22 , 0x0000020); // writes into x22 0000 0000 0000 .... 0001

    model.storeW(0u , 0x00ea50b3 ); // srl x1, x20, x14
    model.storeW(4u , 0x00cad133); // srl x2, x21, x12
    model.storeW(8u , 0x00db5233); // srl x4, x22, x13

    LEMU UPDATE;
    ASSERT_EQ(reg(x1), 0x00aaaaaa);
    UPDATE;
    ASSERT_EQ(reg(x2), 0x0000000);
    UPDATE;
    ASSERT_EQ(reg(x4), 0x00000001);

}

TEST(LEMU, SRA)
{
    Models::MarkI model{Devices::CPU{}, Devices::RAM(8u)}; // init model
    //section for data for shift
    model.write(x10, 0); // writes into x11 0
    model.write(x11, 1); // writes into x24 1
    model.write(x12, 31 ); // writes into x13 32
    model.write(x13 , 5); // writes into x3  5
    model.write(x14 , 8); // writes into x3  5

    model.write(x20 , 0xaaaaaaaa); // writes into x20 1010 1010 1010...
    model.write(x21 , 0x55555555); // writes into x21 0101 0101 0101...
    model.write(x22 , 0x0000020); // writes into x22 0000 0000 0000 .... 0001

    model.storeW(0u , 0x00ea50b3 ); // srl x1, x20, x14
    model.storeW(4u , 0x00cad133); // srl x2, x21, x12
    model.storeW(8u , 0x00db5233); // srl x4, x22, x13

    LEMU UPDATE;
    ASSERT_EQ(reg(x1), 0x00aaaaaa);
    UPDATE;
    ASSERT_EQ(reg(x2), 0x0000000);
    UPDATE;
    ASSERT_EQ(reg(x4), 0x00000001);

}
